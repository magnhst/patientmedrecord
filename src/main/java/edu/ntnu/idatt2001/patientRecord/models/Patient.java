package edu.ntnu.idatt2001.patientRecord.models;

import edu.ntnu.idatt2001.patientRecord.exception.IllegalFormatException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class represent a Patient containing some typical
 * patient fields.
 */
public class Patient {
    //Regex for validating norwegian socialSecurityNumber. Must be exactly 11 digits.
    private static final Pattern socialSecurityNumberRegex = Pattern.compile("^\\d{11}$");

    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPractitioner;

    /**
     * Creates a new instance of patient. Requires that the social
     * security number is not null and an 11 digit number.
     * If the number is not 11 digits then IllegalFormatException is thrown.
     * @param socialSecurityNumber is the patient's social security number
     * @param firstName is the patient's first name
     * @param lastName is the patient's last name
     * @param diagnosis is the patient's diagnosis
     * @param generalPractitioner is the patient's general practitioner
     */
    public Patient(String socialSecurityNumber, String firstName, String lastName, String diagnosis, String generalPractitioner) {
        Objects.requireNonNull(socialSecurityNumber, "SocialSecurityNumber cannot be null.");
        Matcher matcher = socialSecurityNumberRegex.matcher(socialSecurityNumber);
        if (!matcher.find()) {
            throw new IllegalFormatException("SocialSecurityNumber must be 11 digits (Norwegian standard).");
        }
        this.socialSecurityNumber = socialSecurityNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Creates a default empty patient.
     */
    public Patient() {
        this.socialSecurityNumber = "";
        this.firstName = "";
        this.lastName = "";
        this.diagnosis = "";
        this.generalPractitioner = "";
    }

    //Getters
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getDiagnosis() {
        return diagnosis;
    }
    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Patient)) return false;
        Patient patient = (Patient) o;
        return socialSecurityNumber.equals(patient.socialSecurityNumber)
                && firstName.equals(patient.firstName)
                && lastName.equals(patient.lastName)
                && diagnosis.equals(patient.diagnosis)
                && generalPractitioner.equals(patient.generalPractitioner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(socialSecurityNumber, firstName, lastName, diagnosis, generalPractitioner);
    }

    @Override
    public String toString() {
        return "Patient{" +
                "socialSecurityNumber='" + socialSecurityNumber + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", diagnosis='" + diagnosis + '\'' +
                ", generalPractitioner='" + generalPractitioner + '\'' +
                '}';
    }
}
