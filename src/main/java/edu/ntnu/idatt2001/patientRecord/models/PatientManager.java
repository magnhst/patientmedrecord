package edu.ntnu.idatt2001.patientRecord.models;

import edu.ntnu.idatt2001.patientRecord.exception.PatientAlreadyExistException;
import edu.ntnu.idatt2001.patientRecord.exception.PatientDontExistException;
import java.util.HashMap;
import java.util.Objects;

/**
 * Class represent a manager that holds and manages patients.
 */
public class PatientManager {
    // Uses HashMap to easily find Patient objects by their immutable socialSecurityNumber.
    private final HashMap<String, Patient> patients = new HashMap<>();


    /**
     * Adds a patient instance to departments patient register.
     * @param patient is added to patient register.
     * @throws NullPointerException if @param is null
     * @throws PatientAlreadyExistException if @param already exists in the patient register
     * register.
     */
    public void add(Patient patient) throws PatientAlreadyExistException {
        Objects.requireNonNull(patient, "Patient can not be null");
        String socialSecurityNumber = patient.getSocialSecurityNumber();
        if (patients.containsKey(socialSecurityNumber)) {
            throw new PatientAlreadyExistException();
        }

        patients.put(socialSecurityNumber, patient);
    }

    /**
     * Removes Patient instance from its register.
     * @param patient is used as value in register. Indicates which patient should be removed.
     * @throws NullPointerException if @param is null.
     * @throws PatientDontExistException if register cant find @param among the values in the register.
     */
    public void remove(Patient patient) throws PatientDontExistException {
        Objects.requireNonNull(patient, "SocialSecurityNumber can not be null");
        if (!patients.containsValue(patient)) {
            throw new PatientDontExistException();
        }

        patients.remove(patient.getSocialSecurityNumber());
    }

    // Getters
    public HashMap<String, Patient> getPatients() {
        return new HashMap<>(patients);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientManager that = (PatientManager) o;
        return Objects.equals(patients, that.patients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(patients);
    }

    @Override
    public String toString() {
        return "PatientManager{" +
                "patients=" + patients +
                '}';
    }
}
