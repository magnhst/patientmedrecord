package edu.ntnu.idatt2001.patientRecord.exception;

/**
 * IllegalFormatException class extends IllegalArgumentException
 * which makes it an unchecked exception.
 * Should be thrown when a string contains unwanted characters.
 */
public class IllegalFormatException extends IllegalArgumentException{

    /**
     * Creates a new instance of IllegalFormatException with a
     * specified String message.
     * @param message is the message to print with the stacktrace.
     */
    public IllegalFormatException(String message) {
        super(message);
    }

}
