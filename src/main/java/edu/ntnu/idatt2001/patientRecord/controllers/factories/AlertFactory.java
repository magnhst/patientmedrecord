package edu.ntnu.idatt2001.patientRecord.controllers.factories;

import edu.ntnu.idatt2001.patientRecord.utils.PatientRecordAlertTypes;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

/**
 * Represent a factory for alerts. Applies factory method pattern
 * since many alerts are similar with small differences.
 */
public class AlertFactory {
    private Alert alert = new Alert(Alert.AlertType.NONE);

    /**
     * Creates a new alert with specified type, header and content text.
     * @param type is the Alert.AlertType of the alert
     * @param headerText is the header text of the alert
     * @param contentText is the contentText of the alert
     * @return the new alert.
     */
    private Alert createAlert(Alert.AlertType type, String headerText, String contentText){
        alert.setAlertType(type);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        return alert;
    }

    /**
     * Takes inn custom alert types from PatientRecordAlertTypes and
     * gives back a specific alert for that type.
     * @param type is the type of Alert to return.
     *      @see PatientRecordAlertTypes
     * @return the specified alert.
     */
    public Alert getAlertWithType(PatientRecordAlertTypes type){
        switch(type){
            case CLEAR_PATIENTS: return new Alert(Alert.AlertType.CONFIRMATION,
                    "There are patients already in register. Would you like to " +
                            "save these to own file first?",
                    ButtonType.YES,
                    ButtonType.NO
            );
            case IMPORT_FROM_CSV_FAILED: return createAlert(
                    Alert.AlertType.ERROR,
                    "Import from CSV error",
                    "Cannot import from chosen file type. Must be of type CSV."
            );
            case IMPORT_FROM_CSV_PARTIALLY_FAILED: return createAlert(
                    Alert.AlertType.ERROR,
                    "Import from CSV error",
                    "There might exist a column whose fields have been ignored."
                    + " This might be due to wrong column description name in top of imported CSV file, " +
                            "or the file is missing a column."
            );
            case EXPORT_TO_CSV: return createAlert(
                    Alert.AlertType.ERROR,
                    "Export from CSV error",
                    "Could not export patients to CSV."
            );
            case EXIT: return createAlert(
                    Alert.AlertType.CONFIRMATION,
                    "Exit warning",
                    "Are you sure you want to close the application?"
            );
            case PATIENT_ALREADY_EXIST: return createAlert(
                    Alert.AlertType.ERROR,
                    "Patient already exist error",
                    "Social security number already exists in register. Either this patient is already registered, or the number is wrong."
            );
            case DELETE_PATIENT: return createAlert(
                    Alert.AlertType.CONFIRMATION,
                    "Patient delete confirmation",
                    "Are you sure you want to delete this patient?"
            );
            case ILLEGAL_SOCIAL_SECURITY_NUMBER: return createAlert(
                    Alert.AlertType.WARNING,
                    "Social security format warning",
                    "Social security number must be 11 digits (Norwegian standard)."
            );
            case NO_PATIENT_SELECTED: return createAlert(
                    Alert.AlertType.ERROR,
                    "Patient not selected error",
                    "Choose a patient before action."
            );
            case ABOUT: return createAlert(
                    Alert.AlertType.INFORMATION,
                    "Patient Record:\n" +
                    getClass().getModule().getDescriptor().version().orElseThrow(),
                    "A brilliant application created by: Magnus Steensland"
            );
        }
        return null;
    }

    /**
     * Takes inn custom alert types from PatientRecordAlertTypes and
     * gives back a specific alert for that type. Includes an input
     * field for more specific feedback to user.
     * @param type is the type of Alert to return.
     *      @see PatientRecordAlertTypes
     * @return the specified alert.
     */
    public Alert getAlertWithType(PatientRecordAlertTypes type, String input) {
        switch(type){
            case IMPORT_FROM_CSV_PATIENT_ALREADY_EXIST: return createAlert(
                    Alert.AlertType.CONFIRMATION,
                    "Import from CSV error",
                    "Patient with social security number "
                            + input
                    + "already exist. Would you like to replace existing patient?"
            );
            case ILLEGAL_SOCIAL_SECURITY_NUMBER: return createAlert(
                    Alert.AlertType.WARNING,
                    "Social security format warning",
                    "Patient with social security number " +
                            input
                    + " is not 11 digits (Norwegian standard)."
            );
        }
        return null;
    }
}
