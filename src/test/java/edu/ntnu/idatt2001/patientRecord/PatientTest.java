package edu.ntnu.idatt2001.patientRecord;

import edu.ntnu.idatt2001.patientRecord.exception.IllegalFormatException;
import edu.ntnu.idatt2001.patientRecord.models.Patient;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class PatientTest {

    private static Patient patient1, patient2;

    public static void arrange(){
        patient1 = new Patient("26097638798", "Augluin", "Persen", "Cancer", "Synne Sikke");
        patient2 = new Patient("13125334606", "Ben D.", "Lorm", "", "Maren G. Skake");
    }

    @Test
    public void callsConstructor_socialSecurityNumberIsNull_throwsNullPointerException(){
        // Arrange, Act Assert
        assertThrows(NullPointerException.class, () -> new Patient(null, "Augluin", "Persen", "Cancer", "Synne Sikke"));
    }

    @Test
    public void callsConstructor_socialSecurityNumberIsNotElevenDigits_throwsIllegalArgumentException(){
        // Arrange, Act Assert
        assertThrows(IllegalFormatException.class, () ->
                new Patient("260976387980", "Augluin", "Persen", "Cancer", "Synne Sikke"));
        assertThrows(IllegalFormatException.class, () ->
                new Patient("2609763879", "Augluin", "Persen", "Cancer", "Synne Sikke"));
    }

    @Test
    public void callsConstructor_socialSecurityNumberIsNotANumber_throwsIllegalArgumentException(){
        // Arrange, Act Assert
        assertThrows(IllegalFormatException.class, () ->
                new Patient("2609763z8780", "Augluin", "Persen", "Cancer", "Synne Sikke"));
    }
}
