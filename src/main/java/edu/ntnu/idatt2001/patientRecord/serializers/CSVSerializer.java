package edu.ntnu.idatt2001.patientRecord.serializers;

import edu.ntnu.idatt2001.patientRecord.models.Patient;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * Class represent a patientRegister to CSV file serializer.
 * This means it takes in all fields from all patients and
 * systematically writes them in CSV format, using ";" as
 * delimiter.
 */
public class CSVSerializer {
    private final File newFile;

    /**
     * Creates a new Instance of CsVSerializer. Takes in the
     * stage where it opens a file explorer. Sets it fileChooser
     * and its file filters.
     * @param newFile is the file the serializer will serialize to.
     */
    public CSVSerializer(File newFile) {
        this.newFile = newFile;
    }

    /**
     * Creates a single string representing a row to add to the CSV file.
     * Any specified strings that are null, will be written as empty strings.
     * @param column1 is the first column from left to right.
     * @param column2 is the second column from left to right.
     * @param column3 is the third column from left to right.
     * @param column4 is the fourth column from left to right.
     * @param column5 is the fifth column from left to right.
     * @return the whole string as one line/row.
     */
    public String serialize(String column1, String column2, String column3, String column4, String column5) {
        return String.format("%s;%s;%s;%s;%s\n",
                Objects.requireNonNullElse(column1, ""),
                Objects.requireNonNullElse(column2, ""),
                Objects.requireNonNullElse(column3, ""),
                Objects.requireNonNullElse(column4, ""),
                Objects.requireNonNullElse(column5, ""));
    }

    /**
     * Creates a single string representing a patient row to add to the CSV file.
     * Any specified strings that are null, will be written as empty strings.
     * @param patient is the patient to serialize
     * @return a serialized patient.
     */
    public String serialize(Patient patient) {
        return String.format("%s;%s;%s;%s;%s\n",
                Objects.requireNonNullElse(patient.getSocialSecurityNumber(), ""),
                Objects.requireNonNullElse(patient.getFirstName(), ""),
                Objects.requireNonNullElse(patient.getLastName(), ""),
                Objects.requireNonNullElse(patient.getDiagnosis(), ""),
                Objects.requireNonNullElse(patient.getGeneralPractitioner(), ""));
    }


    /**
     * Writes all patients from the specified patient list.
     * @param patientList The list with patients to convert to CSV.
     * @param writer the fileWriter containing the file to write to.
     */
    private void writePatientsToCSV(List<Patient> patientList, FileWriter writer) {
        Objects.requireNonNull(patientList, "PatientList cannot be null");
        Objects.requireNonNull(writer, "Writer cannot be null");
        patientList.forEach(patient -> {
            try {
                writer.write(serialize(patient));
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Creates the file to write the specified patient list to.
     * Writes columns names and patients fields in relation to
     * these names.
     * @param patientList is the patients to write in the file.
     */
    public void writeToCSV(List<Patient> patientList) {
        Objects.requireNonNull(patientList, "PatientList cannot be null");
        if (newFile != null) {

            try (FileWriter writer = new FileWriter(newFile)){

                String row = serialize("socialSecurityNumber", "firstName", "lastName", "diagnosis", "generalPractitioner");
                writer.write(row);
                writer.flush();

                writePatientsToCSV(patientList, writer);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
