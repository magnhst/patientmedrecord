package edu.ntnu.idatt2001.patientRecord.utils;

/**
 * Represent all custom Status types used in PatientRecord
 */
public enum PatientRecordStatusTypes {

    IMPORT_FROM_CSV_FAILED,
    IMPORT_FROM_CSV_SUCCESS,

    EXPORT_TO_CSV_FAILED,
    EXPORT_TO_CSV_SUCCESS,

    ADD_PATIENT_FAILED,
    ADD_PATIENT_SUCCESS,

    DELETE_PATIENT_FAILED,
    DELETE_PATIENT_SUCCESS,

    EDIT_PATIENT_FAILED,
    EDIT_PATIENT_SUCCESS,
}
