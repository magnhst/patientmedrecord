package edu.ntnu.idatt2001.patientRecord.controllers;

import edu.ntnu.idatt2001.patientRecord.controllers.factories.NodeFactory;
import edu.ntnu.idatt2001.patientRecord.models.Patient;
import edu.ntnu.idatt2001.patientRecord.models.PatientManager;
import edu.ntnu.idatt2001.patientRecord.exception.PatientAlreadyExistException;
import edu.ntnu.idatt2001.patientRecord.exception.PatientDontExistException;
import edu.ntnu.idatt2001.patientRecord.controllers.components.PatientDialog;
import edu.ntnu.idatt2001.patientRecord.controllers.factories.AlertFactory;
import edu.ntnu.idatt2001.patientRecord.controllers.factories.StatusFactory;
import edu.ntnu.idatt2001.patientRecord.serializers.CSVDeserializer;
import edu.ntnu.idatt2001.patientRecord.serializers.CSVSerializer;
import edu.ntnu.idatt2001.patientRecord.utils.PatientRecordAlertTypes;
import edu.ntnu.idatt2001.patientRecord.utils.PatientRecordGuiNodeTypes;
import edu.ntnu.idatt2001.patientRecord.utils.PatientRecordStatusTypes;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class represent the main controller of the program.
 */
public class MainController extends VBox {
    private final Stage stage;
    private FileChooser fileChooser;
    private PatientManager manager;

    @FXML VBox contentHolder;
    @FXML TableView<Patient> table;
    @FXML TableColumn<Patient, Label> socialSecurityNumberColumn;
    @FXML TableColumn<Patient, String> firstNameColumn;
    @FXML TableColumn<Patient, String> lastNameColumn;
    @FXML TableColumn<Patient, String> diagnosisColumn;
    @FXML TableColumn<Patient, String> generalPractitionerColumn;
    @FXML StackPane statusBarStackPane;
    @FXML Label statusLabel;

    /**
     * Creates a new instance of mainController with the specified stage.
     * @param stage the stage to use the controller on.
     */
    public MainController(Stage stage) {
        this.stage = stage;
        initiateStage();
        initiateDependentInstances();
    }

    /**
     * Initiates the mainController by loading its fxml view, setting
     * itself as controller and scene.
     */
    public void initiateStage() {
        try {
            FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/edu/ntnu/idatt2001/patientRecord/views/main.fxml"));
            loader.setRoot(this);
            loader.setController(this);
            stage.setScene(new Scene(loader.load()));
        } catch (IOException e){
            throw new RuntimeException(e);
        }
    }

    /**
     * Not currently used since we could use fxml if we wanted.
     *
     * Could be used to create the main window through the NodeFactory.
     * Will require refactoring class to set itself as this VBox and find variable
     * id's created in factory, which would be used in actionMethods.
     */
    private void initiateStageWithFactory() {
        VBox content = (VBox) new NodeFactory().getGuiNode(PatientRecordGuiNodeTypes.VBOX);
        content.getChildren().addAll(
                new NodeFactory().getGuiNode(PatientRecordGuiNodeTypes.MENUBAR),
                new NodeFactory().getGuiNode(PatientRecordGuiNodeTypes.TOOLBAR),
                new NodeFactory().getGuiNode(PatientRecordGuiNodeTypes.TABLEVIEW),
                new NodeFactory().getGuiNode(PatientRecordGuiNodeTypes.STATUSBAR)
        );
    }

    /**
     * Initiates necessary private instances used in the class.
     */
    private void initiateDependentInstances() {
        fileChooser = new FileChooser();
        manager = new PatientManager();

        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        diagnosisColumn.setCellValueFactory(new PropertyValueFactory<>("diagnosis"));
        generalPractitionerColumn.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));
    }

    /**
     * Adds param patient to patient manager and end of tableView.
     * @param patient the patient to add
     * @throws PatientAlreadyExistException if patient is already registered.
     */
    private void addPatientToRegister(Patient patient) throws PatientAlreadyExistException {
        manager.add(patient);
        table.getItems().add(patient);
    }

    /**
     * Removes param patient from patient manager and tableView.
     * @param patient the patient to remove
     * @throws PatientDontExistException if patient is not in register.
     */
    private void removePatientFromRegister(Patient patient) throws PatientDontExistException {
        manager.remove(patient);
        table.getItems().remove(patient);
    }

    /**
     * Removes every patient in register from patient
     * manager and tableView.
     */
    private void removeAllPatientsFromRegister() {
        manager.getPatients().values().forEach(patient ->
        {
            try {
                removePatientFromRegister(patient);
            } catch (PatientDontExistException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Adds all patients from specified list to
     * the the manager and table.
     * Catches PatientAlreadyExistException and gives the
     * user a choice to save either the first or the second
     * occurrence of the patient.
     * @param patientList is the list of patients to add
     */
    private void addPatientListToRegister(List<Patient> patientList) {
        patientList.forEach(patient -> {
            try {
                addPatientToRegister(patient);
            } catch (PatientAlreadyExistException e) {
                new AlertFactory().getAlertWithType(
                        PatientRecordAlertTypes.IMPORT_FROM_CSV_PATIENT_ALREADY_EXIST, patient.getSocialSecurityNumber()).showAndWait()
                        .filter(response -> response == ButtonType.OK)
                        .ifPresent(response -> {
                            try {
                                Patient removablePatient = manager.getPatients().get(patient.getSocialSecurityNumber());
                                removePatientFromRegister(removablePatient);
                                addPatientToRegister(patient);
                            } catch (PatientDontExistException | PatientAlreadyExistException exception) {
                                exception.printStackTrace();
                            }
                        });
            }
        });
    }

    /**
     * Checks if all the patients in the specified patient list
     * lacks a specific patient field. If so it might be due to
     * a typo/error in the CSV file.
     * @param patientList
     */
    private boolean patientFieldsIsMissing(List<Patient> patientList) {
        Set<String> socialSecurityNumbers = new HashSet<>();
        Set<String> firstNames = new HashSet<>();
        Set<String> lastNames = new HashSet<>();
        Set<String> diagnoses = new HashSet<>();
        Set<String> generalPractitioners = new HashSet<>();

        patientList.forEach(patient -> {
            socialSecurityNumbers.add(patient.getSocialSecurityNumber());
            firstNames.add(patient.getFirstName());
            lastNames.add(patient.getLastName());
            diagnoses.add(patient.getDiagnosis());
            generalPractitioners.add(patient.getGeneralPractitioner());
        });

        return socialSecurityNumbers.size() <= 1 ||
                firstNames.size() <= 1 ||
                lastNames.size() <= 1 ||
                diagnoses.size() <= 1 ||
                generalPractitioners.size() <= 1;
    }

    /**
     * Imports a CSV file with data and adds data to table.
     *
     * If the fileChooser is canceled or the patientList
     * read from the file is null, nothing is done.
     *
     * Updates statusbar depending on whether the file was imported
     * or not.
     *
     * Only .CSV files are applicable.
     * @see CSVDeserializer
     */
    @FXML
    public void importFromCSV() {
        if (!manager.getPatients().isEmpty() && !table.getItems().isEmpty()) {
            new AlertFactory().getAlertWithType(PatientRecordAlertTypes.CLEAR_PATIENTS).showAndWait()
                    .filter(response -> response == ButtonType.YES)
                    .ifPresent(response -> {
                        exportToCSV();
                        removeAllPatientsFromRegister();
                    });
        }
        fileChooser.getExtensionFilters().removeAll();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
        fileChooser.setTitle("Open and import CSV file");
        File selectedFile = fileChooser.showOpenDialog(stage);
        if (selectedFile == null) {
            statusLabel.setText(new StatusFactory().getStatusWithType(PatientRecordStatusTypes.IMPORT_FROM_CSV_FAILED));
            return;
        }

        CSVDeserializer deserializer = new CSVDeserializer();
        List<Patient> patientList = deserializer.deserialize(selectedFile);
        if (patientList == null) {
            statusLabel.setText(new StatusFactory().getStatusWithType(PatientRecordStatusTypes.IMPORT_FROM_CSV_FAILED));
            return;
        }

        removeAllPatientsFromRegister();
        addPatientListToRegister(patientList);
        if (patientFieldsIsMissing(patientList)){
            new AlertFactory().getAlertWithType(PatientRecordAlertTypes.IMPORT_FROM_CSV_PARTIALLY_FAILED)
                    .showAndWait();
        }
        statusLabel.setText(new StatusFactory().getStatusWithType(PatientRecordStatusTypes.IMPORT_FROM_CSV_SUCCESS));
    }

    /**
     * Exports the data in the table to a CSV file.
     * If the fileChooser is canceled then nothing is done.
     *
     * Updates statusbar depending on whether the file was exported
     * or not.
     *
     * Only .CSV files are applicable.
     * @see CSVSerializer
     */
    @FXML
    public void exportToCSV(){
        fileChooser.getExtensionFilters().removeAll();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
        fileChooser.setTitle("Create and save CSV file");
        File newFile = fileChooser.showSaveDialog(stage);
        if (newFile == null) {
            statusLabel.setText(new StatusFactory().getStatusWithType(PatientRecordStatusTypes.EXPORT_TO_CSV_FAILED));
            return;
        }

        CSVSerializer serializer = new CSVSerializer(newFile);
        serializer.writeToCSV(new ArrayList<>(manager.getPatients().values()));
        statusLabel.setText(new StatusFactory().getStatusWithType(PatientRecordStatusTypes.EXPORT_TO_CSV_SUCCESS));
    }

    /**
     * Closes the the entire application.
     *
     * Shows a confirmation alert before closing.
     */
    @FXML
    public void closeProgram(){
        new AlertFactory().getAlertWithType(PatientRecordAlertTypes.EXIT)
                .showAndWait()
                .filter(response -> response == ButtonType.OK)
                .ifPresent(response -> Platform.exit());
    }

    /**
     * Adds a patient to the table and storage.
     *
     * Shows alert if patient with similar social security number exists.
     * Shows alert if social security number is not valid.
     */
    @FXML
    public void addNewPatient(){
        PatientDialog window = new PatientDialog(new Patient(), "Patient details - Add");
        Patient patient = null;
        do {
            try {
                patient = window.showAndWait();
                if (patient == null) {
                    return;
                }
                manager.add(patient);
                table.getItems().add(0, patient);
            } catch (PatientAlreadyExistException e) {
                new AlertFactory().getAlertWithType(PatientRecordAlertTypes.PATIENT_ALREADY_EXIST).showAndWait();
                patient = null;
            } catch (NullPointerException exception){
                exception.printStackTrace();
            }
        } while (patient==null);
    }

    /**
     * Deletes a patient from table and storage.
     *
     * Shows alert if no patient is selected.
     * Shows alert to confirm deletion
     */
    @FXML
    public void deletePatient(){
        int index = table.getSelectionModel().getSelectedIndex();
        if (index == -1) {
            new AlertFactory().getAlertWithType(PatientRecordAlertTypes.NO_PATIENT_SELECTED)
                    .showAndWait();
            return;
        }
        ObservableList<Patient> patientList = table.getItems();
        String key = patientList.get(index).getSocialSecurityNumber();
        Patient patient = manager.getPatients().get(key); //Table view get index get SSN

        new AlertFactory().getAlertWithType(PatientRecordAlertTypes.DELETE_PATIENT)
                .showAndWait().filter(response -> response == ButtonType.OK)
                .ifPresent(response -> {
                    try {
                        manager.remove(patient);
                        table.getItems().remove(patient);
                    } catch (PatientDontExistException e) {
                        e.printStackTrace();
                    }
                });
    }

    /**
     * Edits the selected patient
     *
     * Shows an alert if no patient is selected
     * Shows alert if the edited patient's social security number is already registered
     */
    @FXML
    public void editPatient(){
        ObservableList<Patient> patientList = table.getItems();
        int index = table.getSelectionModel().getSelectedIndex();
        if (index == -1) {
            new AlertFactory().getAlertWithType(PatientRecordAlertTypes.NO_PATIENT_SELECTED)
                    .showAndWait();
            return;
        }
        String socialSecurityNumber = patientList.get(index).getSocialSecurityNumber();
        Patient patient = manager.getPatients().get(socialSecurityNumber);
        Patient newPatient = null;

        PatientDialog window = new PatientDialog(patient, "Patient details - Edit");
        do {
            try {
                newPatient = window.showAndWait();

                if (newPatient == null) {
                    return;
                }
                //Switches old patient with new one.
                manager.remove(patient);
                manager.add(newPatient);
                table.getItems().set(index, newPatient);
            } catch (PatientAlreadyExistException e) {
                new AlertFactory().getAlertWithType(PatientRecordAlertTypes.PATIENT_ALREADY_EXIST).showAndWait();
                newPatient = null;
            } catch (PatientDontExistException | NullPointerException e) {
                e.printStackTrace();
            }

        } while (newPatient==null);
    }

    /**
     * Shows an alert with help/about the author info.
     */
    @FXML
    public void showAbout(){
        new AlertFactory().getAlertWithType(PatientRecordAlertTypes.ABOUT).showAndWait();
    }
}
