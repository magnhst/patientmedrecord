package edu.ntnu.idatt2001.patientRecord.utils;

/**
 * Represent all custom AlertTypes used in PatientRecord
 */
public enum PatientRecordAlertTypes {

    IMPORT_FROM_CSV_FAILED,
    IMPORT_FROM_CSV_PARTIALLY_FAILED,
    IMPORT_FROM_CSV_PATIENT_ALREADY_EXIST,
    EXPORT_TO_CSV,
    EXIT,
    ABOUT,
    DELETE_PATIENT,

    ILLEGAL_SOCIAL_SECURITY_NUMBER,

    NO_PATIENT_SELECTED,
    PATIENT_ALREADY_EXIST,

    CLEAR_PATIENTS
}
