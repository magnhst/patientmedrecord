package edu.ntnu.idatt2001.patientRecord.exception;

/**
 * PatientAlreadyExistException class extends Exception which
 * makes it a checked exception. Should be thrown when we
 * try to add a Patient to a collection that already has
 * a patient with similar social security number.
 */
public class PatientAlreadyExistException extends Exception {

    public PatientAlreadyExistException(){
        super("This Patient already exist.");
    }
}
