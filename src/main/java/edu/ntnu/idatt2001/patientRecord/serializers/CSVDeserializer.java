package edu.ntnu.idatt2001.patientRecord.serializers;

import edu.ntnu.idatt2001.patientRecord.controllers.factories.AlertFactory;
import edu.ntnu.idatt2001.patientRecord.exception.IllegalFormatException;
import edu.ntnu.idatt2001.patientRecord.models.Patient;
import edu.ntnu.idatt2001.patientRecord.utils.PatientRecordAlertTypes;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Class represent a CSVDeserializer. It takes a CSV file
 * and deserializes its content into patient fields that
 * can be used to create a patient.
 */
public class CSVDeserializer {
    private String[] columnCategories;
    private HashMap<String, Patient> patients = new HashMap<>();


    /**
     * Creates a hashMap containing the fields of a single patient.
     * Every key represent a CSV file column and each corresponding
     * value represent the corresponding patient field.
     * @param row is the row from which the patients fields originates
     * @return the patient as a map.
     */
    private HashMap<String, String> createPatientAsMap(String row) {
        HashMap<String, String> patientAsMap = new HashMap<>();
        String[] patientValues = row.split(";");
        for (int i = 0; i < columnCategories.length; i++) {
            patientAsMap.put(columnCategories[i], patientValues[i]);
        }
        return patientAsMap;
    }

    /**
     * Deserializes the specified row. The row here is a serialized patient.
     *
     * Creates a new patient from a patient map and adds the new patient to the
     * patients HashMap. If the any patient fields uses illegal format, then a
     * dialog will inform the user of the patient's socialSecurityNumber so as
     * to locate the error in the CSV file.
     *
     * If this happens then null is returned, otherwise the new patietn is returned
     * @param row is a long string containing all the fields of a single patient.
     * @return the new deserialized patient.
     */
    private Patient deserialize(String row) {
        HashMap<String, String> patientAsMap = createPatientAsMap(row);
        Patient patient = null;
        try {
            patient = new Patient(
                    patientAsMap.getOrDefault("socialSecurityNumber", ""),
                    patientAsMap.getOrDefault("firstName", ""),
                    patientAsMap.getOrDefault("lastName", ""),
                    patientAsMap.getOrDefault("diagnosis", ""),
                    patientAsMap.getOrDefault("generalPractitioner", "")
            );
        } catch (IllegalFormatException e) {
            new AlertFactory().getAlertWithType(PatientRecordAlertTypes.ILLEGAL_SOCIAL_SECURITY_NUMBER,
                    patientAsMap.get("socialSecurityNumber"))
                    .showAndWait();
        }
        return patient;
    }

    /**
     * Gets all patients from the the CSV file in the specified reader.
     * Adds columnCategory and associated column field to a hashMap. This
     * HashMap represent a single patient with unordered field names as keys
     * and field content as values to said field names.
     * @param reader the reader who retrieves the rows with single patient data.
     * @throws IOException in case there is an issue with the reader.
     */
    private void putAllPatientsInHashMap(BufferedReader reader) throws IOException {
        String row;
        Patient patient;
        while ((row = reader.readLine()) != null) {
            patient = deserialize(row);
            if (patient != null) {
                patients.put(patient.getSocialSecurityNumber(), patient);
            }
        }
    }

    /**
     * Gets all patients with correct field formats. If no CSV file is
     * chosen, then null is returned.
     *
     * Shows an alert if not all patients from the CSV file is
     * added to the list.
     *
     * @return a list of correctly formatted patients.
     */
    public List<Patient> deserialize(File file) {
        Objects.requireNonNull(file, "File cannot be null");
        Path filePath = Path.of(file.getAbsolutePath());

        try (BufferedReader reader = Files.newBufferedReader(filePath, StandardCharsets.UTF_8)){
            //Sets the column categories as the top row of the CSV file
            columnCategories = reader.readLine().split(";");

            putAllPatientsInHashMap(reader);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ArrayList<>(patients.values());
    }
}
