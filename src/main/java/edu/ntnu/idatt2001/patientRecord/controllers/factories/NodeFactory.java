package edu.ntnu.idatt2001.patientRecord.controllers.factories;

import edu.ntnu.idatt2001.patientRecord.models.Patient;
import edu.ntnu.idatt2001.patientRecord.utils.PatientRecordGuiNodeTypes;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;

/**
 * Represent a factory for GUI nodes. Applies factory method pattern
 * since the task specifically asked for it.
 */
public class NodeFactory {
    private int BUTTON_IMAGE_SIZE = 35;

    /**
     * Creates a new instance of the outer scene holding all the nodes.
     * Sets an ID that can be located in controllers.
     * @return a new VBox as a scene.
     */
    private VBox createVBox() {
        VBox scene = new VBox();
        scene.setId("contentHolder");
        return scene;
    }

    /**
     * Creates the menuBar. This includes creating all menus and
     * menu items, and setting their ID.
     * @return the new menuBar
     */
    private MenuBar createMenubar() {
        MenuItem importCSV = new MenuItem("Import from .CSV");
        MenuItem exportToCSV = new MenuItem("Export to .CSV");
        MenuItem exit = new MenuItem("Exit");

        importCSV.setId("importFromCSV");
        exportToCSV.setId("exportToCSV");
        exit.setId("closeProgram");

        Menu fileMenu = new Menu("File");
        fileMenu.getItems().addAll(importCSV, exportToCSV, new SeparatorMenuItem(), exit);


        MenuItem addPatient = new MenuItem("Add new patient");
        MenuItem deletePatient = new MenuItem("Remove selected patient");
        MenuItem editPatient = new MenuItem("Edit selected patient");

        addPatient.setId("addPatientMenuItem");
        deletePatient.setId("deletePatientMenuItem");
        editPatient.setId("editPatientMenuItem");

        Menu editMenu = new Menu("Edit");
        editMenu.getItems().addAll(addPatient, deletePatient, editPatient);


        MenuItem about = new MenuItem("About");
        about.setId("showAbout");

        Menu helpMenu = new Menu("Help");
        helpMenu.getItems().addAll(about);

        return new MenuBar(fileMenu, editMenu, helpMenu);
    }

    /**
     * Creates the ToolBar. This includes creating all buttons on
     * said toolbar, setting images as their graphics and setting their IDs.
     * @return the new toolbar.
     */
    private ToolBar createToolBar() {
        ImageView addPatientImage = new ImageView(NodeFactory.class.getResource("/edu/ntnu/idatt2001/patientMedRecord/img/user_plus_icon_172126.png").toExternalForm());
        ImageView deletePatientImage = new ImageView(NodeFactory.class.getResource("/edu/ntnu/idatt2001/patientMedRecord/img/user_minus_icon_172127.png").toExternalForm());
        ImageView editPatientImage = new ImageView(NodeFactory.class.getResource("/edu/ntnu/idatt2001/patientMedRecord/img/Edit_User_icon-icons.com_55920.png").toExternalForm());

        addPatientImage.setFitWidth(BUTTON_IMAGE_SIZE);
        addPatientImage.setFitHeight(BUTTON_IMAGE_SIZE);
        deletePatientImage.setFitWidth(BUTTON_IMAGE_SIZE);
        deletePatientImage.setFitHeight(BUTTON_IMAGE_SIZE);
        editPatientImage.setFitWidth(BUTTON_IMAGE_SIZE);
        editPatientImage.setFitHeight(BUTTON_IMAGE_SIZE);

        Button addPatientButton = new Button("add", addPatientImage);
        Button deletePatientButton = new Button("delete", deletePatientImage);
        Button editPatientButton = new Button("edit", editPatientImage);

        addPatientButton.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        deletePatientButton.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        editPatientButton.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

        addPatientButton.setId("addPatientButton");
        deletePatientButton.setId("deletePatientButton");
        editPatientButton.setId("editPatientButton");

        return new ToolBar(addPatientButton, deletePatientButton, editPatientButton);
    }

    /**
     * Creates the TableView. This includes creating all columns on
     * said tableview and setting their IDs.
     * @return the new tableview.
     */
    private TableView<Patient> createTableView() {
        TableColumn<Patient, String> socialSecurityNumberColumn = new TableColumn<>("Social security number (SSN)");
        TableColumn<Patient, String> firstNameColumn = new TableColumn<>("First name");
        TableColumn<Patient, String> lastNameColumn = new TableColumn<>("Last name");
        TableColumn<Patient, String> diagnosisColumn = new TableColumn<>("Diagnosis");
        TableColumn<Patient, String> generalPractitionerColumn = new TableColumn<>("General Practitioner");

        socialSecurityNumberColumn.setId("socialSecurityNumberColumn");
        firstNameColumn.setId("firstNameColumn");
        lastNameColumn.setId("lastNameColumn");
        diagnosisColumn.setId("diagnosisColumn");
        generalPractitionerColumn.setId("generalPractitionerColumn");

        TableView<Patient> patientTableView = new TableView<>();
        patientTableView.setId("patientTableView");
        patientTableView.getColumns().addAll(
                socialSecurityNumberColumn,
                firstNameColumn,
                lastNameColumn,
                diagnosisColumn,
                generalPractitionerColumn
        );

        return patientTableView;
    }

    /**
     * Creates the statusBar. This includes creating a background and label on
     * said statusBar, and setting an ID to the label.
     * @return the new statusBar.
     */
    private StackPane createStatusBar() {
        Rectangle backgroundRectangle = new Rectangle();
        Label statusLabel = new Label("STATUSBAR");
        statusLabel.setId("statusLabel");
        return new StackPane(backgroundRectangle, statusLabel);
    }

    /**
     * Takes inn custom GUI Node types from MainControllerGuiNodes and
     * gives back a specific Node for that type.
     * @param type is the specific Node to return.
     *      @see PatientRecordGuiNodeTypes
     * @return the specified alert.
     */
    public Node getGuiNode(PatientRecordGuiNodeTypes type){
        switch(type){
            case VBOX: return createVBox();
            case MENUBAR: return createMenubar();
            case TOOLBAR: return createToolBar();
            case TABLEVIEW: return createTableView();
            case STATUSBAR: return createStatusBar();
        }
        return null;
    }
}
