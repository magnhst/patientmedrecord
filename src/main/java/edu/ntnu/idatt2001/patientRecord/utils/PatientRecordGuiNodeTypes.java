package edu.ntnu.idatt2001.patientRecord.utils;

/**
 * Represent all custom NodeTypes used in PatientRecord
 */
public enum PatientRecordGuiNodeTypes {
    VBOX,
    MENUBAR,
    TOOLBAR,
    TABLEVIEW,
    STATUSBAR,
}
