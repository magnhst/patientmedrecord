package edu.ntnu.idatt2001.patientRecord.controllers.factories;

import edu.ntnu.idatt2001.patientRecord.models.Patient;
import edu.ntnu.idatt2001.patientRecord.utils.PatientRecordAlertTypes;
import edu.ntnu.idatt2001.patientRecord.utils.PatientRecordStatusTypes;

/**
 * Represent a factory for status strings. Applies factory method pattern.
 */
public class StatusFactory {
    private final String FAILED_CONSTANT = " - FAILED";
    private final String SUCCESS_CONSTANT = " - SUCCESS";

    /**
     * Takes inn custom status types from PatientRecordStatusTypes and
     * gives back a specific status string for that type.
     * @param type is the type of status to return.
     *      @see PatientRecordStatusTypes
     * @param patient the patient who is identified in the method.
     * @return the specified status string.
     */
    public String getStatusWithType(PatientRecordStatusTypes type, Patient patient){
        switch(type){
            case ADD_PATIENT_FAILED:
                return "Patient was not added to register." + FAILED_CONSTANT;
            case ADD_PATIENT_SUCCESS:
                return "Patient with SSN "+ patient.getSocialSecurityNumber() + " was added to register." + SUCCESS_CONSTANT;
            case DELETE_PATIENT_FAILED:
                return "Patient with SSN "+ patient.getSocialSecurityNumber() + " was not deleted from register." + FAILED_CONSTANT;
            case DELETE_PATIENT_SUCCESS:
                return "Patient with SSN "+ patient.getSocialSecurityNumber() + " was deleted from register." + SUCCESS_CONSTANT;
            case EDIT_PATIENT_FAILED:
                return "Patient with SSN "+ patient.getSocialSecurityNumber() + " was not edited." + FAILED_CONSTANT;
            case EDIT_PATIENT_SUCCESS:
                return "Patient with SSN "+ patient.getSocialSecurityNumber() + " was edited." + SUCCESS_CONSTANT;
        }
        return null;
    }

    /**
     * Takes inn custom status types from PatientRecordStatusTypes and
     * gives back a specific status string for that type.
     * @param type is the type of status to return.
     *      @see PatientRecordStatusTypes
     * @return the specified status string.
     */
    public String getStatusWithType(PatientRecordStatusTypes type){
        switch(type){
            case IMPORT_FROM_CSV_FAILED:
                return "Import from CSV file" + FAILED_CONSTANT;
            case IMPORT_FROM_CSV_SUCCESS:
                return "Import from CSV file" + SUCCESS_CONSTANT;
            case EXPORT_TO_CSV_FAILED:
                return "Export to CSV file" + FAILED_CONSTANT;
            case EXPORT_TO_CSV_SUCCESS:
                return "Export to CSV file" + SUCCESS_CONSTANT;
        }
        return null;
    }
}
