package edu.ntnu.idatt2001.patientRecord.exception;

/**
 * PatientDontExistException class extends Exception which
 * makes it a checked exception. Should be thrown when we
 * try to remove a Patient from a collection that is
 * not present in the collection.
 */
public class PatientDontExistException extends Exception {

    public PatientDontExistException(){
        super("This Patient does not exist.");
    }
}
