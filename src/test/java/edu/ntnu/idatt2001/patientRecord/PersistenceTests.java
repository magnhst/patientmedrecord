package edu.ntnu.idatt2001.patientRecord;

import edu.ntnu.idatt2001.patientRecord.exception.PatientAlreadyExistException;
import edu.ntnu.idatt2001.patientRecord.models.Patient;
import edu.ntnu.idatt2001.patientRecord.models.PatientManager;
import edu.ntnu.idatt2001.patientRecord.serializers.CSVDeserializer;
import edu.ntnu.idatt2001.patientRecord.serializers.CSVSerializer;

import static org.junit.jupiter.api.Assertions.*;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PersistenceTests {

    @Nested
    @DisplayName("CsvDeserializer Tests")
    public class CSVDeserializerTest {
        private CSVDeserializer deserializer;
        private PatientManager patientManager;

        @BeforeEach
        public void arrange() {
            this.deserializer = new CSVDeserializer();
            this.patientManager = new PatientManager();
            try {
                patientManager.add(new Patient("12345678912", "Per", "Pål", "Espen", "Askeladden"));
                patientManager.add(new Patient("78945612312", "Harry", "Hole", "Cancer", "Jo Nesbø"));
            } catch (PatientAlreadyExistException e) {
                e.printStackTrace();
            }
        }

        // Positive tests
        @Test
        public void deserializeFile_contentIsLegal_patientListIsReturned() {
            //Arrange
            List<Patient> expectedPatientList = new ArrayList<>(patientManager.getPatients().values());

            //Act
            List<Patient> actualPatientList = deserializer.deserialize(new File("src/test/resources/ExpectedCSVRead.csv"));

            //Assert
            assertEquals(expectedPatientList, actualPatientList);
        }

        // Negative tests
        @Test
        public void deserializeFile_fileIsNull_nullPointerExceptionIsThrown() {
            //Arrange, Act, Assert
            assertThrows(NullPointerException.class, () -> deserializer.deserialize(null));
        }
    }

    @Nested
    @DisplayName("CsvSerializer Tests")
    public class CSVSerializerTest {
        private CSVSerializer serializer;
        private PatientManager patientManager;

        @BeforeEach
        public void arrange() {
            this.serializer = new CSVSerializer(new File("src/test/resources/ActualCSVRead.csv"));
            this.patientManager = new PatientManager();
            try {
                patientManager.add(new Patient("12345678912", "Per", "Pål", "Espen", "Askeladden"));
                patientManager.add(new Patient("78945612312", "Harry", "Hole", "Cancer", "Jo Nesbø"));
            } catch (PatientAlreadyExistException e) {
                e.printStackTrace();
            }
        }

        @AfterEach
        public void clearActualFile() {
            try {
                FileWriter writer = new FileWriter(new File("src/test/resources/ActualCSVRead.csv"));
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Positive tests
        @Test
        public void serializeStrings_argumentIsAllStrings_formattedStringIsReturned() {
            //Arrange
            String expected = "One;Two;Three;Four;Five\n";

            //Act
            String actual = serializer.serialize("One", "Two", "Three", "Four", "Five");

            //Assert
            assertEquals(expected, actual);
        }

        @Test
        public void serializeStrings_argumentIsPatientFields_formattedStringIsReturned() {
            //Arrange
            Patient patient = new Patient(
                    "12345678912",
                    "Per",
                    "Pål",
                    "Espen",
                    "Askeladden"
            );

            String expected = "12345678912;Per;Pål;Espen;Askeladden\n";

            //Act
            String actual = serializer.serialize(patient);

            //Assert
            assertEquals(expected, actual);
        }

        @Test
        public void writeToCSV_patientsAreFoundInCSV() {
            //Arrange
            File expectedFile = new File("src/test/resources/ExpectedCSVRead.csv");

            //Act
            serializer.writeToCSV(new ArrayList<>(patientManager.getPatients().values()));
            File actualFile = new File("src/test/resources/ActualCSVRead.csv");

            //Assert
            try {
                assertTrue(FileUtils.contentEquals(expectedFile, actualFile));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Negative tests
        @Test
        public void serializeStrings_argumentIsNull_formattedStringIsReturned() {
            //Arrange
            String expected = "One;Two;;Four;Five\n";

            //Act
            String actual = serializer.serialize("One", "Two", null, "Four", "Five");

            //Assert
            assertEquals(expected, actual);
        }

        @Test
        public void serializePatient_argumentIsNull_formattedStringIsReturned() {
            //Arrange
            Patient patient = new Patient(
                    "12345678912",
                    "Per",
                    "Pål",
                    null,
                    "Askeladden"
            );

            String expected = "12345678912;Per;Pål;;Askeladden\n";

            //Act
            String actual = serializer.serialize(patient);

            //Assert
            assertEquals(expected, actual);
        }

        @Test
        public void writeToCSV_patientListIsNull() {
            //Arrange, Act, Assert
            assertThrows(NullPointerException.class, () -> serializer.writeToCSV(null));
        }
    }
}
