package edu.ntnu.idatt2001.patientRecord.controllers.components;

import edu.ntnu.idatt2001.patientRecord.exception.IllegalFormatException;
import edu.ntnu.idatt2001.patientRecord.models.Patient;
import edu.ntnu.idatt2001.patientRecord.controllers.factories.AlertFactory;
import edu.ntnu.idatt2001.patientRecord.utils.PatientRecordAlertTypes;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Class represent a patient dialog. It contains all
 * expected patient fields as editable textFields.
 */
public class PatientDialog extends VBox {
    private Stage stage;
    private Patient patient;
    @FXML TextField socialSecurityNumber;
    @FXML TextField firstName;
    @FXML TextField lastName;
    @FXML TextField diagnosis;
    @FXML TextField generalPractitioner;
    @FXML Button saveButton;
    @FXML Button cancelButton;

    /**
     * Creates a new instance of patientDialog.
     * @param patient is the patient to put in the dialog.
     * @param dialogTitle is the title of the dialog window.
     */
    public PatientDialog(Patient patient, String dialogTitle) {
        initiate(dialogTitle);
        initiateEventHandlers();
        this.patient = patient;
        socialSecurityNumber.setText(patient.getSocialSecurityNumber());
        firstName.setText(patient.getFirstName());
        lastName.setText(patient.getLastName());
        diagnosis.setText(patient.getDiagnosis());
        generalPractitioner.setText(patient.getGeneralPractitioner());
    }

    /**
     * Initiates the dialogPane by loading it from an fxml view, setting
     * controllers and scene.
     * @param dialogTitle is the title of the dialog window.
     */
    public void initiate(String dialogTitle) {
        try {
            FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/edu/ntnu/idatt2001/patientRecord/views/components/patient_editor.fxml"));
            loader.setController(this);
            Parent root = loader.load();
            Scene scene = new Scene(root);
            stage = new Stage();
            stage.setTitle(dialogTitle);
            stage.setScene(scene);
        } catch (IOException e){
            throw new RuntimeException(e);
        }
    }

    /**
     * Initiates eventHandlers for the dialogs buttons.
     * This includes saveButton and deleteButton.
     */
    private void initiateEventHandlers(){
        saveButton.addEventHandler(ActionEvent.ACTION, event -> confirmFinished());
        cancelButton.addEventHandler(ActionEvent.ACTION, event -> stage.close());
    }

    /**
     * Tries to create a new patient from the windows textField inputs
     * ans set this to the global patient variable.
     * If the one of the textField strings is formatted illegally,
     * then IllegalFormatException is caught and an alert shown.
     */
    private void confirmFinished(){
        try {
            patient = new Patient(
                    socialSecurityNumber.getText(),
                    firstName.getText(),
                    lastName.getText(),
                    diagnosis.getText(),
                    generalPractitioner.getText()
            );
            stage.close();
        } catch (IllegalFormatException e) {
            new AlertFactory()
                    .getAlertWithType(PatientRecordAlertTypes.ILLEGAL_SOCIAL_SECURITY_NUMBER)
                    .showAndWait();
        }
    }

    /**
     * Opens the patient dialog window and allows the user to give
     * input.
     * @return the patient created from the dialog.
     */
    public Patient showAndWait(){
        patient = null;
        stage.showAndWait();
        return patient;
    }

    // Getter
    public Patient getPatient() {
        return patient;
    }
}
