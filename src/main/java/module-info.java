module patientRecord {
    requires javafx.graphics;
    requires org.controlsfx.controls;
    requires javafx.controls;
    requires javafx.fxml;
    requires org.apache.commons.io;
    requires com.jfoenix;
    requires java.sql;

    opens edu.ntnu.idatt2001.patientRecord to javafx.fxml, javafx.graphics;
    opens edu.ntnu.idatt2001.patientRecord.controllers.components to javafx.fxml;
    opens edu.ntnu.idatt2001.patientRecord.controllers to javafx.fxml;
    opens edu.ntnu.idatt2001.patientRecord.models to javafx.base;

    exports edu.ntnu.idatt2001.patientRecord;

}