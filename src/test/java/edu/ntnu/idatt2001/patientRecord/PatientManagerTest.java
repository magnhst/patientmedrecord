package edu.ntnu.idatt2001.patientRecord;

import edu.ntnu.idatt2001.patientRecord.models.Patient;
import edu.ntnu.idatt2001.patientRecord.models.PatientManager;
import edu.ntnu.idatt2001.patientRecord.exception.PatientAlreadyExistException;
import edu.ntnu.idatt2001.patientRecord.exception.PatientDontExistException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class PatientManagerTest {

    private static PatientManager patientManager;
    private static Patient patient1, patient2, patient3;

    @BeforeEach
    public void arrange() throws PatientAlreadyExistException {
        patientManager = new PatientManager();
        patient1 = new Patient("26097638798", "Augluin", "Persen", "Cancer", "Synne Sikke");
        patientManager.add(patient1);
    }


    //Positive tests
    @Test
    public void add_patientIsNotInDepartment_patientIsAdded() throws PatientAlreadyExistException {
        //Arrange
        Patient expected = new Patient("13125334606", "Ben D.", "Lorm", "", "Maren G. Skake");

        //Act
        patientManager.add(expected);

        //Assert
        assertTrue(patientManager.getPatients().containsValue(expected));
    }

    //Negative tests
    @Test
    public void add_patientIsNull_throwsNullPointerException(){
        // Arrange, Act Assert
        assertThrows(NullPointerException.class, () -> patientManager.add(null));
    }

    @Test
    public void add_patientIsAlreadyInCollection_throwsPatientAlreadyExistException(){
        // Arrange, Act Assert
        assertThrows(PatientAlreadyExistException.class, () ->
                patientManager.add(new Patient("26097638798", "Augluin", "Persen", "Cancer", "Synne Sikke")));
    }



    //Positive tests
    @Test
    public void remove_patientIsInDepartment_patientIsRemove() throws PatientDontExistException {
        //Arrange
        Patient expected = new Patient("26097638798", "Augluin", "Persen", "Cancer", "Synne Sikke");

        //Act
        patientManager.remove(expected);

        //Assert
        assertFalse(patientManager.getPatients().containsValue(expected));
    }

    //Negative tests
    @Test
    public void remove_patientIsNull_throwsNullPointerException(){
        // Arrange, Act Assert
        assertThrows(NullPointerException.class, () -> patientManager.remove((Patient) null));
    }

    @Test
    public void remove_patientIsNotInCollection_throwsPatientDontExistException(){
        // Arrange, Act Assert
        assertThrows(PatientDontExistException.class, () ->
                patientManager.remove(new Patient("13125334606", "Ben D.", "Lorm", "", "Maren G. Skake")));
    }
}
