package edu.ntnu.idatt2001.patientRecord;

import edu.ntnu.idatt2001.patientRecord.controllers.MainController;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * JavaFX App. The main Class for running the application.
 */
public class Main extends Application {

    @Override
    public void start(Stage stage) {
        stage.getIcons().add(new Image(getClass().getResource("/edu/ntnu/idatt2001/patientRecord/img/journal-icon.jpg").toExternalForm()));
        new MainController(stage);
        stage.setTitle("PatientRecord");
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}